from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from django.shortcuts import get_object_or_404
from django.views import View
from django.views.generic.base import TemplateView
from django.contrib import messages

from datetime import timedelta, date, datetime
from administrator.models import EmployeeDetails
from administrator.forms import EmployeeForm


class LandingPage(TemplateView):
    template_name = "admin/home/employee_form.html"

    @method_decorator(login_required)
    def get(self, request):
        context = {}
        return render(request, self.template_name, context)

    @method_decorator(login_required)
    def post(self, request):
        response_dict = {}
        form = EmployeeForm(request.POST)
        if form.is_valid():
            employee = form.save()
            messages.success(request, "Added successfully")
            return redirect("add-salary", employee.id)
        else:
            response_dict["reason"] = get_error(form)
            messages.error(request, response_dict["reason"])
        return redirect(request.GET.get("return") or "landing-page")


class ListEmployee(TemplateView):
    template_name = "admin/home/home.html"

    @method_decorator(login_required)
    def get(self, request):
        context = {}
        employees = EmployeeDetails.objects.filter(is_active=True).order_by("-id")
        context["employee"] = employees
        return render(request, self.template_name, context)


class AddSalary(TemplateView):
    template_name = "admin/home/salary.html"

    @method_decorator(login_required)
    def get(self, request, pk):
        context = {}
        employees = EmployeeDetails.objects.get(id=pk)
        context["employee"] = employees
        return render(request, self.template_name, context)

    @method_decorator(login_required)
    def post(self, request, pk):
        response_dict = {}
        current_salary = request.POST.get("current_salary")
        employees = EmployeeDetails.objects.get(id=pk)
        employees.current_salary = current_salary
        messages.success(request, "Updated successfully")
        employees.save()
        return redirect("list-employee")


class DeleteEmployee(TemplateView):
    @method_decorator(login_required)
    def post(self, request, pk):
        response_dict = {}
        employees = EmployeeDetails.objects.get(id=pk)
        employees.is_active = False
        employees.save()
        messages.success(request, "Deleted successfully")
        return redirect("list-employee")


class EditEmployee(TemplateView):
    @method_decorator(login_required)
    def post(self, request, pk):
        response_dict = {}
        current_salary = request.POST.get("current_salary")
        employee = EmployeeDetails.objects.get(id=pk)
        form = EmployeeForm(request.POST, instance=employee)
        if form.is_valid():
            employee = form.save()
            employee.current_salary = current_salary
            employee.save()
            messages.success(request, "Updated successfully")
        else:
            response_dict["reason"] = get_error(form)
            messages.error(request, response_dict["reason"])
        return redirect(request.GET.get("return") or "landing-page")