from django.conf.urls import include, url
from administrator import views

urlpatterns = [
    url(r"^landing-page/$", views.LandingPage.as_view(), name="landing-page"),
    url(r"^list-employee/$", views.ListEmployee.as_view(), name="list-employee"),
    url(
        r"^edit-employee/(?P<pk>\d+)/$",
        views.EditEmployee.as_view(),
        name="edit-employee",
    ),
    url(
        r"^delete-employee/(?P<pk>\d+)/$",
        views.DeleteEmployee.as_view(),
        name="delete-employee",
    ),
    url(r"^add-salary/(?P<pk>\d+)/$", views.AddSalary.as_view(), name="add-salary"),
]