from django.views import View
from django.shortcuts import render, redirect


class Home(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect("landing-page")
        else:
            return render(request, "public/login.html")
