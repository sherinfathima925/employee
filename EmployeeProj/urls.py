from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.views.static import serve

urlpatterns = [
    path("admin/", admin.site.urls),
    path("user/", include("user.urls")),
    path("accounts/", include("allauth.urls")),
    path("", include("common.urls")),
    path(
        "administrator/",
        include(("administrator.urls")),
    ),
]

if settings.DEBUG:
    urlpatterns += [
        path(
            "media/(?P<path>.*)",
            serve,
            {
                "document_root": settings.MEDIA_ROOT,
            },
        ),
    ]
