from django.db import models


class EmployeeDetails(models.Model):

    first_name = models.CharField(null=False, blank=False, max_length=255)
    employee_code = models.CharField(null=True, blank=True, max_length=255)
    email = models.EmailField(null=False, blank=False)
    last_name = models.CharField(null=True, blank=True, max_length=255)
    mobile_no = models.CharField(
        max_length=30, verbose_name="Mobile number", null=True, blank=True
    )
    current_salary = models.FloatField(default=0)

    is_active = models.BooleanField(null=False, blank=True, default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
