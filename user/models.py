import random, string, re, json
from datetime import datetime

from django.db import models
from django.db.models import Manager
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils import timezone

USER_TYPE_CHOICES = (("SUPER_ADMIN", "Super Admin"),)


class UserProfile(AbstractUser):
    # username
    # email
    # password
    # first_name
    gender = models.CharField(max_length=10, null=False, default="MALE")
    user_type = models.CharField(
        max_length=30, null=False, blank=True, choices=USER_TYPE_CHOICES
    )
    mobile_no = models.CharField(
        max_length=30, verbose_name="Mobile number", null=True, blank=True
    )

    status = models.CharField(max_length=50, null=False, blank=True, default="ACTIVE")
    is_active = models.BooleanField(null=False, blank=True, default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
