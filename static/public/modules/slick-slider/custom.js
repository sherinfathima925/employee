// slick_slider_1
$('.slick_slider_1').slick({
  infinite: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: true,
  arrows: false,
});

// slick_slider_2
$('.slick_slider_2').slick({
  infinite: false,
  slidesToShow: 2,
  slidesToScroll: 1,
  dots: false,
  arrows: true,
  responsive: [
  {
    breakpoint: 767,
    settings: {
     slidesToShow: 1,
    },
  },
 ],
});

// slick_slider_3
$('.slick_slider_3').slick({
  infinite: false,
  slidesToShow: 3,
  slidesToScroll: 1,
  dots: false,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 3000,
  pauseOnFocus: false,
  pauseOnHover: false,
  pauseOnDotsHover: false,
  responsive: [
  {
    breakpoint: 767,
    settings: {
     slidesToShow: 1,
    },
  },
 ],
});