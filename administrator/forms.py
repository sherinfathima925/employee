from django.forms import ModelForm
from administrator.models import EmployeeDetails


class EmployeeForm(ModelForm):
    class Meta:
        model = EmployeeDetails
        exclude = ["updated", "created", "is_active", "current_salary"]
